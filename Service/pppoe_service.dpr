program pppoe_service;

uses
  SvcMgr,
  RASUnit in 'RASUnit.pas',
  SetupAPI in 'SetupAPI.pas',
  pppoe_autoconnect in 'pppoe_autoconnect.pas' {PPPoEAutoconnect: TService};

{$R ..\icon.RES}

begin
  Application.Initialize;
  Application.CreateForm(TPPPoEAutoconnect, PPPoEAutoconnect);
  Application.Run;
end.
