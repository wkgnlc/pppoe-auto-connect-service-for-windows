unit pppoe_autoconnect;

interface

uses
  Windows, Messages, SysUtils, SetupAPI, Classes, Graphics, RASUnit, Controls, ComCtrls, SvcMgr, Dialogs,registry,
  ExtCtrls,ShellAPI;

type
  TPPPoEAutoconnect = class(TService)
    timer_connect: TTimer;
    procedure timer_connectTimer(Sender: TObject);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceExecute(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
  private
    { Private declarations }
  public
  hRas: ThRASConn;
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;
TMyDialParam = Record
   AMsg:Integer;
    AState:TRasConnState;
    AError:Integer;
  end;
  
var
  PPPoEAutoconnect: TPPPoEAutoconnect;
  MyDialParam:TMyDialParam;
  errord: Integer;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  PPPoEAutoconnect.Controller(CtrlCode);
end;

function TPPPoEAutoconnect.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;


procedure EnableNetDevice(aState:boolean;index:integer);
var
  NetPnPHandle:HDEVINFO;
  PCHP:TSPPropChangeParams;
  DeviceData:TSPDevInfoData;
begin
  NetPnPHandle:=SetupDiGetClassDevs(@GUID_DEVCLASS_NET, 0, 0, DIGCF_PRESENT);
  if NetPnPHandle=INVALID_HANDLE_VALUE then exit;

  DeviceData.cbSize:=sizeof(TSPDevInfoData);
  SetupDiEnumDeviceInfo(NetPnPHandle, index, DeviceData);

  PCHP.ClassInstallHeader.cbSize:=sizeof(TSPClassInstallHeader);

  if SetupDiSetClassInstallParams(NetPnPHandle,@DeviceData,@PCHP,sizeof(TSPPropChangeParams)) then
   begin
    PCHP.ClassInstallHeader.cbSize := sizeof(TSPClassInstallHeader);
    PCHP.ClassInstallHeader.InstallFunction := DIF_PROPERTYCHANGE;
    PCHP.Scope := DICS_FLAG_CONFIGSPECIFIC;
    if aState then
     PCHP.StateChange := DICS_ENABLE
              else
     PCHP.StateChange := DICS_DISABLE;
    SetupDiSetClassInstallParams(NetPnPHandle,@DeviceData,@PCHP,sizeof(TSPPropChangeParams));
    SetupDiCallClassInstaller(DIF_PROPERTYCHANGE,NetPnPHandle,@DeviceData);
   end;
  DeviceData.cbSize := sizeof(TSPDevInfoData);
  SetupDiDestroyDeviceInfoList(NetPnPHandle);
end;

procedure EnableALLNET();
var
  NetPnPHandle: HDEVINFO;
  DeviceNumber:DWORD;
  DevData: TSPDevInfoData;
  DeviceInterfaceData: TSPDeviceInterfaceData;
  RES:BOOL;
begin
  NetPnPHandle := SetupDiGetClassDevs(@GUID_DEVCLASS_NET, nil, 0, DIGCF_PRESENT );
  if NetPnPHandle = INVALID_HANDLE_VALUE then  Exit;
  DeviceNumber := 0;
  repeat
   DeviceInterfaceData.cbSize := SizeOf(TSPDeviceInterfaceData);
   DevData.cbSize := SizeOf(TSPDevInfoData);
   RES := SetupDiEnumDeviceInfo(NetPnPHandle, DeviceNumber, DevData);
   if RES then
    begin
     EnableNetDevice(true,DeviceNumber);
     Inc(DeviceNumber);
    end;
  until not RES;
  SetupDiDestroyDeviceInfoList(NetPnPHandle);
end;

procedure CreatePPPoE(name:string);
var
 i: integer;
   BuffSize          : Integer;
  Entries           : Integer;
  Entry             : Array[1..MaxEntries] of TRasEntryName;
  X,Result_         : Integer;
  AllEntries        : TStrings;
  RE: TRasEntry;
  params: TRasDialParams;
  elec: integer;
begin
try
elec:=-1;
Fillchar(RE, sizeof(TRasEntry), 0);
RE.dwSize := sizeof(TRasEntry);
RE.dwfOptions := 1024262939;
RE.dwfNetProtocols := RASNP_Ip;
RE.dwFramingProtocol := RASFP_Ppp;
RE.szDeviceType := 'pppoe';
RE.szDeviceName := 'WAN Miniport (PPPoE)';
RasSetEntryPropertiesA(nil, PAnsiChar(name), @RE, sizeof(TRasEntry), nil, 0);
params.dwSize := sizeof(TRasDialParams);
StrPCopy(params.szEntryName, name);
except

end;

end;
procedure writelog(str: string);
var
  logFile:TextFile;
  ntime: TDateTime;
  p,t: string;
begin
  t:=GetEnvironmentVariable('TEMP');
  ntime := Now;
  p:=t+'\pppoe.log';
  AssignFile(logFile, p);
  if FileExists(p) then
    Append(logFile)
  else
    Rewrite(logFile);
  WriteLn(logFile, DateToStr(ntime)+' '+TimeToStr(ntime)+': '+str);
  CloseFile(logFile);
end;

procedure TPPPoEAutoconnect.timer_connectTimer(Sender: TObject);
var
  Registry: TRegistry;
  SEInfo: TShellExecuteInfo;
  ExitCode: DWORD;
  ExitCodeStr,t,p: string;
  Name_conn, User, Password: string;
begin
  Registry := TRegistry.Create;
  Registry.RootKey := HKEY_LOCAL_MACHINE;
  Registry.OpenKey('software\callpppoe',true);
  t:=GetEnvironmentVariable('TEMP');
  p:=t+'\pppoe.log';
  Registry.WriteString('tmpfile', p);
  Name_conn:=Registry.ReadString('Name_conn');
  User:=Registry.ReadString('User');
  Password:=Registry.ReadString('Password');
  if(Name_conn='') or (User='') or (Password='') then begin
    if(errord=0) then
      writelog('������: �� ��������� ������� ������');
    errord := 1;
  end else errord := 0;
  Registry.CloseKey;
  Registry.Free;
  timer_connect.Enabled:=false;
  FillChar(SEInfo, SizeOf(SEInfo), 0) ;
  SEInfo.cbSize := SizeOf(TShellExecuteInfo) ;
  with SEInfo do begin
    fMask := SEE_MASK_NOCLOSEPROCESS;
    lpFile := 'rasdial';
    lpParameters := PChar(Name_conn+' '+User+' '+Password) ;
    nShow := SW_HIDE;
  end;
  if ShellExecuteEx(@SEInfo) then begin
    repeat
        Sleep(100);
        GetExitCodeProcess(SEInfo.hProcess, ExitCode) ;
    until (ExitCode <> STILL_ACTIVE);
    if(ExitCode>0) then begin
      OutputDebugString(PAnsiChar('terminated errorcode: '+inttostr(ExitCode)));
      ExitCodeStr:=IntToStr(ExitCode);

      if(ExitCode=691) then
        writelog('������: '+ExitCodeStr+' ������� �����: "'+User+'", ������: "'+Password+'", ��������� ��.')
      else
        writelog('������: '+ExitCodeStr);
      CreatePPPoE(Name_conn);
      EnableALLNET();
      sleep(10000);
    end;
  end else
    OutputDebugString('Error starting rasdial!');
  timer_connect.Enabled:=True;
end;


procedure TPPPoEAutoconnect.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
 Stopped := True;
end;

procedure TPPPoEAutoconnect.ServiceExecute(Sender: TService);
begin
 while not Terminated do
    ServiceThread.ProcessRequests(True);
end;

procedure TPPPoEAutoconnect.ServiceStart(Sender: TService; var Started: Boolean);
var
  logFile:TextFile;
  ntime: TDateTime;
  p,t: string;
begin
  t:=GetEnvironmentVariable('TEMP');
  ntime := Now;
  p:=t+'\pppoe.log';
  AssignFile(logFile, p);
  Rewrite(logFile);
  CloseFile(logFile);
  Started:=True;
  EnableALLNET();
end;

end.
