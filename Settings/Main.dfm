object SettingsForm: TSettingsForm
  Left = 579
  Top = 235
  Width = 577
  Height = 406
  AutoSize = True
  BorderStyle = bsSizeToolWin
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' PPPoE '#1072#1074#1090#1086#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl4: TLabel
    Left = 8
    Top = 96
    Width = 82
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089' '#1089#1083#1091#1078#1073#1099':'
    Color = clBtnFace
    ParentColor = False
  end
  object status: TLabel
    Left = 96
    Top = 96
    Width = 3
    Height = 13
  end
  object grp1: TGroupBox
    Left = 0
    Top = 0
    Width = 569
    Height = 81
    Caption = #1044#1072#1085#1085#1099#1077
    TabOrder = 0
    object lbl1: TLabel
      Left = 24
      Top = 24
      Width = 34
      Height = 13
      Caption = #1051#1086#1075#1080#1085':'
    end
    object lbl2: TLabel
      Left = 152
      Top = 24
      Width = 41
      Height = 13
      Caption = #1055#1072#1088#1086#1083#1100':'
    end
    object lbl3: TLabel
      Left = 280
      Top = 24
      Width = 96
      Height = 13
      Caption = #1048#1084#1103' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103':'
    end
    object save_button: TButton
      Left = 424
      Top = 40
      Width = 75
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
      Enabled = False
      TabOrder = 0
      OnClick = save_buttonClick
    end
    object User: TEdit
      Left = 24
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
      OnChange = UserChange
    end
    object Password: TEdit
      Left = 152
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 2
      OnChange = PasswordChange
    end
    object Name_conn: TEdit
      Left = 280
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 3
      Text = 'pppoe'
      OnChange = Name_connChange
    end
  end
  object startbutton: TButton
    Left = 168
    Top = 88
    Width = 121
    Height = 25
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100'/'#1057#1090#1072#1088#1090
    TabOrder = 1
    OnClick = startbuttonClick
  end
  object stopbutton: TButton
    Left = 288
    Top = 88
    Width = 113
    Height = 25
    Caption = #1057#1090#1086#1087'/'#1091#1076#1072#1083#1080#1090#1100
    TabOrder = 2
    OnClick = stopbuttonClick
  end
  object log: TListBox
    Left = 0
    Top = 120
    Width = 569
    Height = 257
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 3
  end
  object tmr1: TTimer
    Interval = 1
    OnTimer = tmr1Timer
    Left = 96
    Top = 160
  end
  object tmr2: TTimer
    OnTimer = tmr2Timer
    Left = 352
    Top = 136
  end
end
