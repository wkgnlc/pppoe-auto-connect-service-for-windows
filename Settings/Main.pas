unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Registry, StdCtrls, ExtCtrls, WinSvc, ShellAPI;

type
  TSettingsForm = class(TForm)
    grp1: TGroupBox;
    save_button: TButton;
    User: TEdit;
    Password: TEdit;
    Name_conn: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    tmr1: TTimer;
    startbutton: TButton;
    stopbutton: TButton;
    lbl4: TLabel;
    status: TLabel;
    tmr2: TTimer;
    log: TListBox;
    procedure save_buttonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UserChange(Sender: TObject);
    procedure PasswordChange(Sender: TObject);
    procedure Name_connChange(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure checkservice;
    procedure startbuttonClick(Sender: TObject);
    procedure stopbuttonClick(Sender: TObject);
    procedure tmr2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SettingsForm: TSettingsForm;
  size: Integer;

implementation

{$R *.DFM} {$R file_service.RES}

procedure TSettingsForm.save_buttonClick(Sender: TObject);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  Registry.RootKey := HKEY_LOCAL_MACHINE;
  Registry.OpenKey('software\callpppoe',true);
  Registry.WriteString('User', User.Text);
  Registry.WriteString('Password', Password.Text);
  Registry.WriteString('Name_conn', Name_conn.Text);
  Registry.CloseKey;
  Registry.Free;
  save_button.Enabled:=False;
end;

function ServiceGetStatus(sMachine, sService: PChar): DWORD;
var
  SCManHandle, SvcHandle: SC_Handle;
  SS: TServiceStatus;
  dwStat: DWORD;
begin
  dwStat := 0;
  // Open service manager handle.
  SCManHandle := OpenSCManager(sMachine, nil, SC_MANAGER_CONNECT);
  if (SCManHandle > 0) then
  begin
    SvcHandle := OpenService(SCManHandle, sService, SERVICE_QUERY_STATUS);
    // if Service installed
    if (SvcHandle > 0) then
    begin
      // SS structure holds the service status (TServiceStatus);
      if (QueryServiceStatus(SvcHandle, SS)) then
        dwStat := ss.dwCurrentState;
      CloseServiceHandle(SvcHandle);
    end;
    CloseServiceHandle(SCManHandle);
  end;
  Result := dwStat;
end;

function ServiceRunning(sMachine, sService: PChar): Boolean;
begin
  Result := SERVICE_RUNNING = ServiceGetStatus(sMachine, sService);
end;

procedure TSettingsForm.checkservice;
begin
  if(ServiceRunning(nil, 'PPPoEAutoconnect')) then  begin
    startbutton.Enabled:=False;
    stopbutton.Enabled:=True;
    status.Caption:='��������';
    status.Font.Color:=clGreen;
  end else begin
    startbutton.Enabled:=True;
    stopbutton.Enabled:=False;
    status.Caption:='�� ��������';
    status.Font.Color:=clRed;
  end;
end;

procedure TSettingsForm.FormCreate(Sender: TObject);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  Registry.RootKey := HKEY_LOCAL_MACHINE;
  Registry.OpenKey('software\callpppoe',true);
  User.Text:=Registry.ReadString('User');
  Password.Text:=Registry.ReadString('Password');
  if(Registry.ReadString('Name_conn')<>'') then
    Name_conn.Text:=Registry.ReadString('Name_conn')
  else Name_conn.Text:='pppoe';
  Registry.CloseKey;
  Registry.Free;
  checkservice;
end;






procedure TSettingsForm.UserChange(Sender: TObject);
begin
save_button.Enabled:=True;
end;

procedure TSettingsForm.PasswordChange(Sender: TObject);
begin
save_button.Enabled:=True;
end;

procedure TSettingsForm.Name_connChange(Sender: TObject);
begin
save_button.Enabled:=True;
end;

procedure TSettingsForm.tmr1Timer(Sender: TObject);
var
  p: string;
  buf: string[80];
  logFile: TextFile;
  Registry: TRegistry;
  tsize: Integer;
begin
  tmr1.Interval:=1000;
  Registry := TRegistry.Create;
  Registry.RootKey := HKEY_LOCAL_MACHINE;
  Registry.OpenKey('software\callpppoe',true);
  p:=Registry.ReadString('tmpfile');
  Registry.CloseKey;
  Registry.Free;
  if(FileExists(p)) then begin
    try
      AssignFile(logFile,p);
      Reset(logFile);
          while not EOF(logFile) do
          begin
            tsize:=tsize+1;
            readln(logfile, buf);
            if(size<tsize) then
              log.ItemIndex := log.Items.Add(buf);
          end;
          size:=tsize;
      CloseFile(logFile);
     except
    end;
  end;
end;

procedure S_StopService(ServiceName: string);
var
  schService, schSCManager: DWORD;
  p: PChar;
  ss: _SERVICE_STATUS;
begin
  p := nil;
  schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if schSCManager = 0 then
    RaiseLastWin32Error;
  try
    schService := OpenService(schSCManager, PChar(ServiceName),
      SERVICE_ALL_ACCESS);
    if schService = 0 then
      RaiseLastWin32Error;
    try
      if not ControlService(schService, SERVICE_CONTROL_STOP, SS) then
        RaiseLastWin32Error;
    finally
      CloseServiceHandle(schService);
    end;
  finally
    CloseServiceHandle(schSCManager);
  end;
end;

procedure S_StartService(ServiceName: string);
var
  schService,
    schSCManager: Dword;
  p: PChar;
begin

  p := nil;
  schSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if schSCManager = 0 then
    RaiseLastWin32Error;
  try
    schService := OpenService(schSCManager, PChar(ServiceName),
      SERVICE_ALL_ACCESS);
    if schService = 0 then
      RaiseLastWin32Error;
    try
      if not Winsvc.startService(schService, 0, p) then
        RaiseLastWin32Error;
    finally
      CloseServiceHandle(schService);
    end;
  finally
    CloseServiceHandle(schSCManager);
  end;
end;

procedure installservice(fname: string; uninstall: Boolean);
var
 SEInfo: TShellExecuteInfo;
 ExitCode: DWORD;
begin
    FillChar(SEInfo, SizeOf(SEInfo), 0) ;
    SEInfo.cbSize := SizeOf(TShellExecuteInfo) ;
    with SEInfo do begin
      fMask := SEE_MASK_NOCLOSEPROCESS;
      lpFile := PChar(fname);
      if(uninstall=True) then
        lpParameters := PChar('/uninstall /silent')
      else
        lpParameters := PChar('/install /silent');
      nShow := SW_HIDE;
    end;
    if ShellExecuteEx(@SEInfo) then begin
      repeat
          Sleep(100);
          GetExitCodeProcess(SEInfo.hProcess, ExitCode) ;
      until (ExitCode <> STILL_ACTIVE);
      if(ExitCode>0) then begin
        OutputDebugString(PAnsiChar('terminated errorcode: '+inttostr(ExitCode)));
      end;
    end;
end;

procedure TSettingsForm.startbuttonClick(Sender: TObject);
var
 rStream: TResourceStream;
 fStream: TFileStream;
 fname,t: string;
begin
    tmr1.Enabled:=False;
    tmr2.Enabled:=false;
    size:=0;
    log.Items.Clear;
    startbutton.Enabled:=False;
    t:=GetEnvironmentVariable('SYSTEMROOT');
    fname := t+'/pppoe_service.exe';
    rStream := TResourceStream.Create(hInstance, 'pppoeService', RT_RCDATA) ;
    try
      fStream := TFileStream.Create(fname, fmCreate) ;
      try
        fStream.CopyFrom(rStream, 0) ;
      finally
        fStream.Free;
      end;
    finally
      rStream.Free;
    end;
    try
    S_StopService('PPPoEAutoconnect');
    installservice(fname, true);
    except

    end;
    installservice(fname, false);
    S_StartService('PPPoEAutoconnect');
    tmr1.Enabled:=True;
    tmr2.Enabled:=True;
end;

procedure TSettingsForm.stopbuttonClick(Sender: TObject);
var
 fname,t: string;
begin
     tmr1.Enabled:=false;
     tmr2.Enabled:=false;
     stopbutton.Enabled:=False;
     t:=GetEnvironmentVariable('SYSTEMROOT');
     fname := t+'/pppoe_service.exe';
     S_StopService('PPPoEAutoconnect');
     installservice(fname, true);
     DeleteFile(fname);
     tmr1.Enabled:=true;
     tmr2.Enabled:=true;
end;

procedure TSettingsForm.tmr2Timer(Sender: TObject);
begin
  checkservice;
end;

end.
